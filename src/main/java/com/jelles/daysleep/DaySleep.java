package com.jelles.daysleep;

import com.jelles.daysleep.commands.SleepCommand;
import org.bukkit.plugin.java.JavaPlugin;

public final class DaySleep extends JavaPlugin {

    @Override
    public void onEnable() {
        getCommand("daysleep").setExecutor(new SleepCommand(this));
    }

    @Override
    public void onDisable() {

    }
}
