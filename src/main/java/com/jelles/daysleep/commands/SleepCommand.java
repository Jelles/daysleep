package com.jelles.daysleep.commands;

import com.jelles.daysleep.DaySleep;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SleepCommand implements CommandExecutor {

    private DaySleep daySleep;

    public SleepCommand(DaySleep daySleep) {
        this.daySleep = daySleep;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0 || args.length > 2) {
            CommandInfo(sender);
        } else if (args[0].equals("sleep")) {
            if (args[1] != null) {
                if (Bukkit.getPlayer(args[1]) != null) {
                    // SLEEP PLAYER
                    return true;
                }
                sender.sendMessage("Invalid player name!");
                CommandInfo(sender);
                return true;
            }
            CommandInfo(sender);
            return true;
        }
        return false;
    }

    private void CommandInfo(CommandSender sender) {
        String commandInfo = "\n-----------------------§7§l[DAYSLEEP]§r-----------------------\n" +
                "§b/daysleep help:§r Shows all the commands and information\n" +
                "§b/daysleep sleep <player>:§r Sets the player in sleep state.";
        sender.sendMessage(commandInfo);

    }
}
